# Závislosti projektu - npm, webpack, Gulp, Grunt

## `npm`

- správce závislostí (balíčků) pro *node.js*
- manifest a seznam závislostí v `package.json`
- balíčky v adresáři `node_modules`

## konfigurace `package.json`

```json
{
  "name": "bi-pjs.1-tutorials",
  "version": "0.0.1",
  "description": "Reference project for BI-PJS.1 in spring semester 2018.",
  "license": "MIT",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/bi-pjs.1/tutorials"
  },
  "dependencies": {
    "react": "^16.3.0",
    "react-dom": "^16.3.0"
  },
  "devDependencies": {
    "webpack": "^4.4.1",
    "webpack-dev-server": "^3.1.1",
    "webpack-cli": "^2.0.13",
    "html-webpack-plugin": "^3.1.0"
  }
}
```
- předchozí ukázka obsahuje minimální konfiguraci, tak aby `npm install` prošlo bez varování
- existuje spoustu dalších [nastavení](https://docs.npmjs.com/files/package.json)

## vlastnosti `npm` 

- široká funkcionalita
- možnost instalace balíčků lokálně nebo globálně `-g`
- spouštění vlastních skriptů při vývoji
  - lokální webserver
  - minifikace
  - testy
- instalace balíčku je pomalá → `yarn`

## použití `npm`

- `npm init` - vytvoření `package.json`
- `npm install` - instalace závislostí (lze také použít zkraceně `npm i`)
- `npm i X@Y` - instalace závislosti `X` ve verzi `Y`
- `npm install X --save` - instalace závislosti `X` s uložením do `package.json`
- `npm run ...` - spouštění skriptů
- `npm install X -g` - globální instalace závislosti `X`

## `Yarn`

- stejně jako `npm` to je správce závislostí (balíčků) pro *node.js*
- manifest a seznam zavislosti je také v `package.json`, závislosti jsou nainstalovány také v `node_modules`
- kdykoliv je modul přidán, yarn vytvoří nebo aktualizuje `yarn.lock`, což je analogie k `npm-shrinkwrap.json`
- velká výhoda `yarnu` oproti `npm` je paralelní instalace závislostí

## použití `Yarn`

- `yarn init` - vytvoření `package.json`
- `yarn add X` - instalace závislosti `X` rovnou přidá do `package.json` údaj o balíčku - analogie k `npm i X --save`
- `yarn add X@Y` - instalace závislosti `X` ve verzi `Y`
- `yarn run ...` - spouštění skriptů
- `yarn global install X` - v `Yarn` se řeší prefixem a ne `--global` flagem jako v `npm`

## webpack

- slouží k vytváření balíčků z modulárního kódu pro použití v prohlížeči
- konfiguruje se v souboru `webpack.config.js`
- konfigurační soubor obsahuje 4 základní klíče:
    - `entry` obsahuje nastavení vstupu Webpacku,
    - `output` obsahuje nastavení výstupu Webpacku,
    - `module` obsahuje nastavení práce nad moduly (zejména nastavení **loaderů**),
    - `plugins` obsahuje pluginy a jejich nastavení.

### `entry`

- vstup odkud má `webpack` vytvářet balíčky
- každý soubor je považován za kořen stromu závislostí
- může obsahovat několik typů:
    - string - cesta k souboru => jeden entry point, jeden balíček
    - pole - pole cest k souborům => více entry pointů, jeden balíček
    - object - objekt pojmenovaných cest k souborům => více entry pointů, více balíčků
- **Pravidlo: jeden entry point na HTML stránku, SPA = jeden globální entry point, MPA = více entrypointů**

### `output`
- nastavení výstupu Webpacku
- objekt, jehož nejdůležitější klíče jsou:
    - `path` - cesta ke složce pro výstup
    - `file` - název výstupního souboru

### `module`/Loadery

- nastavují se pravidla, kdy se má loader aplikovat
- pravidlo se skládá z testu regulárním výrazem
- loader má název a volitelně další parametry
- pomocí loaderů lze  např. transpilovat JSX na ES5 js, ES6 js na ES5 js, Sass na CSS...
- **vždy musí být výsledkem posledního loaderu v pipě JS**

### `plugin`

- transformace per bundle
- používají se pro funkcionalitu, kterou nemůže loader, např:
    - extrakce stylů,
    - vyčlenění vendorů do samostatných balíčků,
    - minifikace a uglifikace JS

### `webpack.config.js`

```javascript
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = {
    entry: './src/app.js',
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'dist')
    },
    mode: 'development',
    plugins: [
        new HtmlWebpackPlugin({template: './index.html'})
    ]
};
```

## `webpack-dev-server`

- vývojový server pro rozběhnutí aplikace při vývoji
- umožňuje sledování souborů na filesystému a automaticky při změně provede opětovnou kompilaci `--watch`
- sekci `devServer` lze přidat do ukázkové konfigurace výše jako další atribut k `entry`, `output` ...
- 
```javascript
const config = {
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        port: 9000
    }
}
```

## Gulp

- automatizace rutinních činností při vývoji
- *task runner*
- konfigurace úloh v `gulpfile.js`
- instalace

```bash
npm install gulp gulp-cli -D
touch gulpfile.js
```

## použití `gulp`

- v `gulpfile.js` jsou definovány úlohy (task), které lze spouštět pomocí

```bash
gulp název
```

- pluginy - minifikace JS a CSS

```bash
npm install gulp-concat gulp-rename gulp-uglify gulp-minify-css -D
```

- pluginy se načítají pomocí `require()`
- vstup/výstup jednotlivých kroků úlohy se řetězí pomocí `pipe()`

```javascript
var gulp = require('gulp');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

gulp.task('min', function() {
    return gulp.src('js/script.js')
        .pipe(rename('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'))
    ;
});
```

## Grunt

- další *task runner*
- konfigurace v `Gruntfile.js` (nebo `Gruntfile.coffee`)
- instalace

```bash
npm install grunt grunt-cli -D
```

## použítí Grunt

- v `Gruntfile.js` je konfigurace a definice úloh, spuštění

```bash
grunt název
```

- pluginy - minifikace JS a CSS

```bash
npm install grunt-contrib-uglify grunt-contrib-cssmin -D
```

- ruční vytvoření `Gruntfile.js`

```javascript
module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/<%= pkg.name %>.js',
        dest: 'build/<%= pkg.name %>.min.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerTask('default', ['uglify']);
};
```

## gulp vs Grunt

- sledování souborů (watch) je v gulp vestavěno, Grunt potřebuje plugin
- gulp pluginy dělají jednu věc, Grunt pluginy jsou komplexnější
- gulp úlohy jsou JavaScript, Grunt úlohy jsou JSON
- gulp podporuje proudové zpracování, Grunt ukládá mezi jednotlivými kroky

## Zdroje

- [npm](https://docs.npmjs.com/)
- [webpack](https://webpack.js.org/)
- [Webpack – moderní Web Development](https://www.ackee.cz/blog/moderni-web-development-webpack/)
- [gulp](http://gulpjs.com/)
- [Grunt](http://gruntjs.com/)
- [Concatenate & Minify Javascript with Gulp, Improve Site Performance](http://codehangar.io/concatenate-and-minify-javascript-with-gulp/)
- [An Introduction to Gulp.js](https://www.sitepoint.com/introduction-gulp-js/)
