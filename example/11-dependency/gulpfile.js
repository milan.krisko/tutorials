var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var minifyCSS = require('gulp-minify-css');

var jsFiles = [
        'bower_components/jquery/dist/jquery.js',
        'js/script.js'
    ],
    jsDest = 'gulp',
    cssFiles = [
        'bower_components/bootstrap/dist/css/bootstrap.css',
        'css/style.css'
    ],
    cssDest = 'gulp';

gulp.task('run', ['scripts', 'css']);

gulp.task('scripts', function() {
    return gulp.src(jsFiles)
        // spojeni (concatenate)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(jsDest))

        // minifikace
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(jsDest))
    ;
});

gulp.task('css', function() {
    return gulp.src(cssFiles)
        // spojeni (concatenate)
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(cssDest))

        // minifikace
        .pipe(rename('styles.min.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest(cssDest))
    ;
});
