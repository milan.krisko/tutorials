# React Props, State a Lifecycle

## Props

- komponenty podporují props bez ohledu na to, jestli se vytváří pomocí funkce nebo pomocí třídy
- props jsou určené pouze ke čtení, není možné je upravovat
- mohou obsahovat i funkce, toho se využívá pro obsluhu událostí

    ```jsx
    function Welcome(props) {
        return <h1>Hello {props.name}!</h1>;
    }

    class Welcome extends React.Component {
        constructor(props) {
            super(props);
        }

        render() {
            return (<h1>Hello {this.props.name}!</h1>);
        }
    }

    ReactDOM.render(<Welcome name="Martin" />, document.getElementById('app'));
    ```

## State

- v reálném světe nám nestačí pouze něco číst, ale potřebujeme i měnit stav
- z tohoto důvodů mají komponenty, které jsou definovány jako třída atribut `state`
- nastavuje se v konstruktoru, např. `this.state = {loggedIn: false}`
- další nastavení za běhu se provádí metodou `setState()`
    - povinným argumentem je `object` se změneným stavem, např `setState({loggedIn: true})`
    - druhou variantou je předat jako povinný argument funkci s následující notací `(prevState, props) => stateChange`

        ```jsx
            this.setState((prevState, props) => {
                return {counter: prevState.counter + props.step};
            });
        ```
- změna stavu se sloučí s přechozím stavem
- stav je přístupný pouze v dané komponentě a neví o něm ani potomci ani rodič
- potomkům je však stav nebo jeho část možno předat pomocí props

## Lifecycle

- každá komponenta má životní cyklus, který má několik fází:
    - mounting - je vytvořena a vložena do DOM instance komponenty
    - updating - změna props nebo state, způsobující přerenderování
    - unmounting - odstranění komponenty z DOM
    - error handling - nastane chyba práci s danou komponentou
- pro jednotlivé fáze životního cyklu má komponenta sadu metod
    - mounting:
        - `static getDerivedStateFromProps(nextProps, prevState)`
        - `componentDidMount()` - často se používa pro voláni AJAX requestu 
    - updating
        - `static getDerivedStateFromProps(nextProps, prevState)`
        - `componentDidUpdate(prevProps, prevState, snapshot)`
        - `shouldComponentUpdate(nextProps, nextState)` - díki tyhle metodě, můžete zlepšit výkon aplikace a to tak, že nastavíte podmínky kdy se bude komponenta rerendrovat - návhratová hodnota je boolean
    - unmount
        - `componentWillUnmount()` - metoda, kde je vhodný odebrat event listenery, timeout funkce, případně pročistit data v reduxu určené pro komponentu
    - error handling - `componentDidCatch(error, info)`
- od verze 16.3.0 jsou následující metody deprecated:
    - `componentWillMount()`
    - `componentWillReceiveProps()`
    - `componentWillUpdate()`
    - do verze 17 se dají použít s prefixem `UNSAFE_`
- v [dokumentaci](https://reactjs.org/docs/react-component.html#the-component-lifecycle) lze nalézt další metody

## Zpracování událostí

- obsluha událostí je podobná jako v HTML s několika rozdíly
    - React události jsou camelCase a ne lowercase
    - jako handler se posílá funkce a ne string

        ```jsx
        function activateLasers() {
            console.log('Lasers are on!');
        }
        
        // HTML
        <button onclick="activateLasers()">
          Activate Lasers
        </button>
        
        // React
        <button onClick={activateLasers}>
          Activate Lasers
        </button>
        ```
    - také není možné zabránit výchozímu chování pomocí `return false;`, ale je třeba volat explicitně `preventDefault()`

        ```jsx
        // HTML
        <a href="#" onclick="console.log('The link was clicked.'); return false">
          Click me
        </a>

        // React
        function ActionLink() {
            function handleClick(e) {
                e.preventDefault();
                console.log('The link was clicked.');
            }

            return (
                <a href="#" onClick={handleClick}>
                  Click me
                </a>
            );
        }
        ```
- pokud je použita komponenta formou třídy, která má handler např. `handleClick()` je třeba použít v konstruktoru následující volání
`this.handleClick = this.handleClick.bind(this);`
- v dokumentaci naleznete také alternativy jak zachovat kontext `this` v handleru bez předchozího volání
- pokud potřebujete do handleru předat další informace můžete použít jedno z následujících volání

    ```jsx
    <button onClick={(e) => this.deleteRow(id, e)}>Delete Row</button>
    <button onClick={this.deleteRow.bind(this, id)}>Delete Row</button>
    ```

## PropTypes

- slouží pro kontrolu datových typů jednotlivých props u komponent
- odděleno do samostatného balíčku `prop-types`, který je potřeba instalovat
- definunje se pomocí objektů, kde atributem je název prop a hodnoutou její typ

    ```jsx
    Welcome.propTypes = {
        name: PropTypes.string
    };
    ```
- existuje varianta, která způsobí varování, pokud daná prop není nastavena

    ```jsx
    Welcome.propTypes = {
        name: PropTypes.string.isRequired
    };
    ```
- existuje také možnost nastavit výchozí hodnoty jednotlivých props

    ```jsx
    Welcome.defaultProps = {
      name: 'Stranger'
    };
    ```

## React.Fragment

- pokud z komponenty potřebujeme vrátit kolekci elementů můžeme použít pole

    ```jsx
    render() {
      return [
        <li key="A">First item</li>,
        <li key="B">Second item</li>,
        <li key="C">Third item</li>,
      ];
    }
    ```
- nebo se může použít speciální komponenta `<React.Fragment>`, která se "nerenderuje" a u statických částí není potřeba
používat klíče
    
    ```jsx
    render() {
      return (
        <React.Fragment>
          <li>First item</li>
          <li>Second item</li>
          <li>Third item</li>
        </React.Fragment>
      );
    }
    ```

## Zdroje
- [Component and Props - React](https://reactjs.org/docs/components-and-props.html)
- [State and Lifecycle - React](https://reactjs.org/docs/state-and-lifecycle.html)
- [React.Component - React](https://reactjs.org/docs/react-component.html)
- [Handling Events - React](https://reactjs.org/docs/handling-events.html)
- [Typechecking With PropTypes - React](https://reactjs.org/docs/typechecking-with-proptypes.html)
