module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build: {
                src: [
                    'bower_components/jquery/dist/jquery.js',
                    'js/script.js'
                ],
                dest: 'grunt/scripts.min.js'
            }
        },
        cssmin: {
            target: {
                files: {
                    'grunt/styles.min.css': [
                        'bower_components/bootstrap/dist/css/bootstrap.css',
                        'css/style.css'
                    ]
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    // Default task(s).
    grunt.registerTask('default', ['uglify', 'cssmin']);

};