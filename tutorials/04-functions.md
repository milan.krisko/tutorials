# Funkce - cvičení

> [Teoretická část](../04-functions.md)

1. Napište funkci, která bude fungovat jako počítadlo
    - Použijte `closure`, umožněte více samostatných počítadel

      ```javascript
      var a = f(), b = f();
      a()  // 1
      a()  // 2
      b()  // 1
      ```
    - Pokuste se dosáhnout stejné funkcionality jiným způsobem, nepoužívejte globální proměnnou dostupnou vně funkce
    
      ```javascript
      f()   // 1
      f()   // 2
      ```
1. Napište funkci, která má proměnný počet argumentů a vrací aritmetický průměr a medián z numerických hodnot.
1. **Napište funkci, která rozhodne, zda zadané číslo je [Keithovo číslo](https://cs.wikipedia.org/wiki/Fibonacciho_posloupnost#Repfigity)**.
   Za pomoci této funkce vypište vyhovující čísla menší než zadaný limit.