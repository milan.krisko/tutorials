# React

- JS knihovna pro vytváření UI
- neprogramuje se jak se má aplikace chovat, ale jak vypadá výsledek
- používají se tzv. komponenty

## Instalace

```shell
npm init
npm install --save react react-dom
```

## Rozšíření JSX

- `const element = <h1>Hello, world!</h1>;`
- předchozí ukázka není ani HTML ani string
- jedná se o syntaktické rozšíření JS, kterým se vytváří komponenty
- JSX spojuje dohromady logiku zobrazení a obsluhy událostí
- místo oddělování technologií (např. HTML a JS), rozděluje na komponenty jednotlivé části UI
- pomocí `{}` můžete vložit do JSX libovolný JS výraz

    ```jsx
    function formatName(user) {
      return user.firstName + ' ' + user.lastName;
    }

    const user = {
      firstName: 'Harper',
      lastName: 'Perez'
    };

    const element = (
      <h1>
        Hello, {formatName(user)}!
      </h1>
    );
    ```

- JSX je samo o sobě výrazem, dá se tedy použít v JS

    ```jsx
    function getGreeting(user) {
      if (user) {
        return <h1>Hello, {formatName(user)}!</h1>;
      }
      return <h1>Hello, Stranger.</h1>;
    }
    ```

- atributy se píšou camelCase `className`
- hierarchická struktura, tedy JSX tagy mohou obsahovat potomky
- JSX automaticky ošetřuje uživatelský vstup

    ```jsx
    const title = response.potentiallyMaliciousInput;
    // Tohle je bezpečné
    const element = <h1>{title}</h1>;
    ```

## Tvorba a využití komponent

- pomocí funkce nebo třídy

    ```jsx
    function Welcome() {
        return <h1>Hello world!</h1>;
    }
    ```

    ```jsx
    class Welcome extends React.Component {
        render() {
            return (<h1>Hello world!</h1>);
        }
    }
    ```

    ```javascript
    ReactDOM.render(<Welcome />, document.getElementById('app'));
    ```

- můžeme zanořovat komponenty do sebe
- komponenty mohou mít různé atributy dle potřeby v atributu props

    ```jsx
    function Welcome(props) {
        return <h1>Hello {props.name}!</h1>;
    }

    class Welcome extends React.Component {
        constructor(props) {
            super(props);
        }

        render() {
            return (<h1>Hello {this.props.name}!</h1>);
        }
    }

    ReactDOM.render(<Welcome name="Martin" />, document.getElementById('app'));
    ```

## Spuštění aplikace pomocí `webpack`

1. instalace potřebných závislostí `npm install -D babel-core babel-loader babel-preset-env babel-preset-react babel-preset-stage-1`
1. vytvoření `.babelrc` souboru
1. použití `babel-loader` jako webpack pluginu (úprava konfigurace ve `webpack.config.js`)
1. spustění aplikace

## "Alternativa" k `webpack` - `react-scripts`

1. instalace balíčku `react-scripts`: `npm install --save react-scripts`
1. přidání/úprava sekce `scripts` v `package.json`

    ```
    "scripts": {
        "start": "react-scripts start"
    }
    ```
1. sestavení aplikace se potom provádí pomocí `npm start`, které ve skutečnosti používá přednastavený webpack tj. je potřeba splnit předpoklady:
   - výchozí HTML soubor umístěte do `public/index.html`
   - výchozí JS entrypoint umístěte do `src/index.js`

## Zdroje
- [React](https://reactjs.org/)
- [Learn Webpack for React](https://medium.freecodecamp.org/learn-webpack-for-react-a36d4cac5060)
