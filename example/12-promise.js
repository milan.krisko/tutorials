function multi(a, b) {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(a * b), 1000)
})
}

multi(4, 5).then((data) =>
multi(data, 6)
).then((data) => {
    console.log('Hotovo', data);
})