# Built-in funkce

## `eval`

- vyhodnotí kód zadaný jako řetězec
- nebezpečná!
- při nepřímém volání pracuje v globálním kontextu

```javascript
function test() {
  var x = 2, y = 4;
  console.log(eval("x + y")); // 6
  var geval = eval;
  console.log(geval("x + y")); // ReferenceError because x is undefined
}
```
- nestandardní inverzní funkce `uneval`

## `isFinite`, `isNaN`

- `isFinite` - `false` pro +/- `Infinity`, `NaN`, `undefined` nebo `{}`
- `isNaN` - `true` pro `NaN`, `undefined` nebo `{}`

## `parseInt`

- explicitní převod `string` na `int`
- `parseInt(string [, radix])`

```javascript
parseInt("123")      // 123
parseInt("123", 8)  // 83
parseInt("123", 16)  // 291
parseInt("F123")     // NaN
parseInt("F123", 8) // NaN
parseInt("F123", 16) // 61731
parseInt("0123")     // 123,   radix == 10; může být i 8 - implementačně závislé !!!
parseInt("0xF123")   // 61731, radix == 16
```
## `parseFloat`

- explicitní převod `string` na `float`
- `parseFloat(string)`

```javascript
parseFloat("2.72")     // 2.72
parseFloat("2.72abcd") // 2.72
parseFloat("2.72e+3")  // 2720
parseFloat("a2")       // NaN
```

## `encodeURI`, `decodeURI`

- `encodeURI` - zakóduje znaky pomocí UTF-8 escape sekvence, `decodeURI` naopak
- `encodeURI` předpokládá, že vstupem je celá URL
- `encodeURIComponent` - zakóduje celý vstup, vhodnější pro query string apod.

```javascript
encodeURI('https://ondřej.caletka.cz')          // 'https://ond%C5%99ej.caletka.cz'
encodeURIComponent('https://ondřej.caletka.cz') // 'https%3A%2F%2Fond%C5%99ej.caletka.cz'
encodeURI('test[]="Žluťoučký kůň"')  // 'test%5B%5D=%22%C5%BDlu%C5%A5ou%C4%8Dk%C3%BD%20k%C5%AF%C5%88%22'
```

# Built-in objekty

## `Array` (pole)

- [MDN Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)

```javascript
var mesta = [ 'Praha', 'Brno' ];
mesta.length           // 2
mesta[1]               // 'Brno'
mesta.indexOf('Praha') // 0
```
- iterace `mesta.forEach(function(item, index, array)) {});`
- úprava pomocí `splice(start [, deleteCount [, item1 [, item2, ... ]]])`

```javascript
var mesta = [ 'Praha', 'Brno', 'Ostrava', 'Olomouc' ];
mesta.splice(2, 1, 'Třinec');
// odstraní 1 prvek od pozice 2 a přidá tam nový 'Třinec'
// vrací ['Ostrava']
// mesta: [ 'Praha', 'Brno', 'Třinec', 'Olomouc' ]
```

### Kopírování pole
- `array.slice([begin [, end]])` - mělká kopie, pro objekty kopíruje referenci, ne včetně `end`

```javascript
var a = [ 1, 2, 3, 4 ];
var b = a.slice(1,3); // [2, 3]
```

```javascript
var a = { p: 1, q: 2 };
var b = [ a, 10]; // b: [ { p: 1, q: 2 }, 10 ]
c = b.slice();    // c: [ { p: 1, q: 2 }, 10 ]
a.p = 3;
console.log(b);  // [ { p: 3, q: 2 }, 10 ]
console.log(c);  // [ { p: 3, q: 2 }, 10 ]
```

### Pole jako fronta nebo zásobník

- zásobník na začátku pole

```javascript
mesta.unshift('Aš');       // [ 'Aš', 'Praha', 'Brno' ]
var prvni = mesta.shift(); // první: 'Aš', mesta: [ 'Praha', 'Brno' ]
```

- zásobník na konci pole

```javascript
mesta.push('Ostrava');      // [ 'Praha', 'Brno', 'Ostrava' ]
var posledni = mesta.pop(); // posledni: 'Ostrava', mesta: [ 'Praha', 'Brno' ]
```

- fronta - `push`, `shift`

### `map` a `filter`
- s callbackem

```javascript
var a = [ 1, 2, 3, 4 ];
var b = a.map(function(i) { return i * i; });
// [ 1, 4, 9, 16 ]
var c = a.filter(function(i) { return i % 2 == 0; });
// [ 2, 4 ]
```

## `Object`
- všechny objekty jsou odvozeny od `Object`
- inicializace literálem

```javascript
var a = { a: 10, b: 'ahoj' }; // literál
Object.keys(a)                // [ 'a', 'b' ]
Object.values(a)              // [ 10, 'ahoj' ]
a.hasOwnProperty('b')         // true
```
- inicializace konstruktorem - podle hodnoty vytvoří objekt daného typu

```javascript
var a = new Object(5);    // Number: 5
var b = new Object(true); // Boolean: true
typeof a                  // 'object'
```

### Vybrané metody `Object`

```javascript
var a = { a: 10, b: 'ahoj' }; // literál
```

```javascript
Object.keys(a)    // [ 'a', 'b' ]
Object.values(a)  // [ 10, 'ahoj' ]
Object.entries(a) // [ [ 'a', 10 ], [ 'b', 'ahoj' ] ]
```
### Vybrané metody `Object.prototype`
- tyto metody „dědí“ každý objekt
- je možné nastavit vlastní (override)

```javascript
a.hasOwnProperty('b') // true
a.toString()          // '[object Object]'
a.toString = function() { return this.a + ', ' + this.b; }
a.toString()          // '10, ahoj'
```

## `Error`

- `new Error([message[, fileName[, lineNumber]]])`
- typy chyb
  - `RangeError` - číslo je mimo platné meze
  - `ReferenceError` - např. při odkazu na neexistující proměnnou
  - `SyntaxError` - syntaktická chyba
  - `TypeError` - hodnota je neočekávaného typu
  - ...

### Použití `Error`

```javascript
try {
    throw new TypeError('Došlo k chybě', 'error.js', 2)
} catch(e) {
    console.log(e instanceof TypeError);
    // true
    console.log(e.message, e.name, e.fileName, e.lineNumber, e.columnNumber, e.stack);
    // 'Došlo k chybě', 'TypeError', 'error.js', 2, 0, '.....'
}
```

- Pozn. `fileName` a `lineNumber` není standardizováno

## Objekty pro datové typy

### `Number`
- atributy `EPSILON`, `MAX_VALUE`, `MIN_VALUE`, ...
- metody `isNaN()`, `isFinite()`, `isInteger()`, `parseInt()`, `parseFloat()`, ...

```javascript
Number.EPSILON    // 2.220446049250313e-16
```

### `Date`
- více variant konstruktoru

```javascript
new Date();
new Date(value);
new Date(dateString);
new Date(year, month[, date[, hours[, minutes[, seconds[, milliseconds]]]]]);
```
- metody `now()`, `parse()`, ...
- metody instancí `getDate()`, `getHours()`,  `setDate()`, `setHours()`, `toDateString()`, ...

### Měření času

```javascript
var zacatek = new Date();
pracuj();
var konec = new Date();
console.log(konec - zacatek); // v ms
```

### `Math`
- obsahuje matematické konstanty a funkce
- atributy `E`, `LN2`, `LN10`, `LOG2E`, `LOG10E`, `PI`, `SQRT1_2`, `SQRT2`
- metody `abs(x)`, `sqrt(x)`, `floor(x)`, `ceil(x)`, `round(x)`, `trunc(x)`, `random()`,
  `min(x, [, y [, ...]])`, `max(x, [, y [, ...]])`, ...

### `String`
- literálem `'Ahoj'`, `"Ahoj"`
- objektově `String(x)` - `x` bude převedeno na řetězec
- escape sekvence `\0`, `\n`, `\xXX`, `\uXXXX` ...

### Práce s řetězcem

```javascript
var s = 'Ahoj';
s.length    // 4
s[1]        // h
s.charAt(1) // h
s[1] < h[2] // true, 'h' < 'o'
s + '!'     // 'Ahoj!'
```

### Metody `String`

```javascript
String.fromCharCode(74, 83) // 'JS'
```
- metody instance `indexOf()`, `match()`, `replace()`, `slice()`, `split()`,
  `startsWith()`, `endsWith()`, `toLowerCase()`, `toUpperCase()`, `trim()`, ...

### `RegExp`
- literálem `/vzor/parametry`
- objektově `new RegExp('vzor', parametry)`

```javascript
var re = /\w+/;
var re = new RegExp('\\w+');
'Ahoj'.match(re)        // [ 'Ahoj', index: 0, input: 'Ahoj' ]
'Ahoj světe'.match(re)  // [ 'Ahoj', index: 0, input: 'Ahoj světe' ]
'Ahoj světe'.replace(/[^\s]/g, '-') // '---- -----'
'1+2-3+4'.split(/\+|-/)  // [ '1', '2', '3', '4' ]
```

## `Map`
- asociativní pole (key-value)
- klíč může být funkce, objekt nebo jednoduchý datový typ
- lze snadno zjistit velikost
- klíč se porovnává pomocí `===`
- `Object`
  - má některé výchozí klíče (atributy), hrozí kolize
  - klíč může být jen `String` nebo `Symbol`

### Atributy a metody instance `Map`
- atributy - `size`
- metody `delete(key)`, `get(key)`, `has(key)`, `set(key, val)`,
  `clear()`, `keys()`, `values()`, `entries()`, `forEach(callback)`

```javascript
var m = new Map();
m.set('A', 'Praha');
m.set('B', 'Brno');
m.set('C', 'České Budějovice');
```

### Převod z/na `Array`

```javascript
var pole = [ [ 'A', 'Praha' ], [ 'B', 'Brno' ], [ 'C', 'České Budějovice' ] ];
var m = new Map(pole);
var p = [ ...m ]; // spread operátor
var p = Array.from(m);
```

### Iterace `Map`

```javascript
for (var [k, v] of m) {
    console.log(k + ' = ' + v);
}
// A = Praha
// B = Brno
// C = České Budějovice
```

```javascript
for (var [k, v] of m.entries()) {
    console.log(k + ' = ' + v);
}
// A = Praha
// B = Brno
// C = České Budějovice
```

### Iterace `Map`

```javascript
for (var k of m.keys()) {
    console.log(k);
}
// A
// B
// C
```

```javascript
for (var v of m.values()) {
    console.log(v);
}
// Praha
// Brno
// České Budějovice
```

### Iterace `Map`

```javascript
m.forEach(function(v, k) { // pořadí value, key!
    console.log(k + ' = ' + v);
});
// Praha
// Brno
// České Budějovice
```

## `Set`
- množina
- unikátní hodnoty jednoduchého typu nebo objekt
- [množinové operace je nutné implementovat](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set#Implementing_basic_set_operations)


- Atributy a metody instance `Map`
  - atributy - `size`
  - metody `delete(val)`, `has(val)`, `add(val)`,
    `clear()`, `keys()`, `values()`, `entries()`, `forEach(callback)`

### Unikátnost prvků

```javascript
var s = new Set();
var o = {a: 1, b: 2};
s.add(o);
s.add({a: 1, b: 2});     // lze, protože odkazuje na jiný objekt
s.values()               // Set { { a: 1, b: 2 }, { a: 1, b: 2 } }
s.has(o)                 // true
s.has({ a: 1, b: 2 })    // false !
```

### Iterace `Set`
- `keys()` i `values()` - vrací stejný výsledek

```javascript
var pole = [ 1, 'ahoj', 2 ];
var s = new Set(pole);
// nasledující vypisuje stejný výsledek
for (var i of s) console.log(i);
for (var i of s.keys()) console.log(i);
for (var i of s.values()) console.log(i);
```

### Převod z/na `Array`

```javascript
var pole = [ 1, 'ahoj', 2 ];
var s = new Set(pole);
var p = [ ...s ]; // spread operátor
var p = Array.from(s);
```

## JSON
- *JavaScript Object Notation*
- narozdíl od JavaScriptu v JSON
  - názvy atributů musí být v uvozovkách
  - nesmí být čárka za posledním prvkem
  - povinná číslice za desetinnou čárkou

### Metody `JSON`
- metody
  - `JSON.parse` - převod z JSON řetězce na JS hodnotu
  - `JSON.strigify` - převod z JS hodnoty na JSON řetězec

```javascript
var x = JSON.parse('{"a": 5, "b": "ahoj"}');  // { a: 5, b: 'ahoj' }
x.c = 10;
var s = JSON.stringify(x);                    // '{"a":5,"b":"ahoj","c":10}'

// nemusí obsahovat objekt
var x = JSON.parse('5e+1');                   // 50
```

## Zdroje

- [MDN Predefined functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions#Predefined_functions)
- [MDN Standard built-in objects](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects)
  - [MDN Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)
  - [MDN Object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object)
  - [MDN Error](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error)
  - [MDN RegExp](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp)
  - [MDN Map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)
  - [MDN Set](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set)
  - [MDN JSON](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON)
