# Výjimky a chyby

## Výjimky

- `try` ... `catch`, `throw`

- `throw` lze použít na libovolný typ proměnné, ale obvykle se používají existující výjimky

```javascript
throw 'Nastala obecná chyba';
throw 42;
```

## Vyhození objektu

```javascript
try {
    throw { toString: function() {
        return 'Narušena kontinuita vesmíru'; }
    };
} catch(e) {
    console.log('Chyba: ' + e);
}
```

```javascript
function EnemyError(code) {
    this.message = 'Nepřítel na obzoru';
    this.code = code;
}

try {
    throw new EnemyError(13);
} catch(e) {
    console.log('Chyba #' + e.code + ': ' + e.message);
}
```

## try ... catch ... finally

- `finally` se provádí po `try` nebo `catch`
- nezávisle na tom, zda k výjimce dojde, nezávisle na tom, zda je odchycena
- `finally` může potlačit `return` v `catch`

```javascript
function f() {
    try {
        console.log(0);
        throw new Error;
    } catch(e) {
        console.log(1);
        return true;    // čeká se na výsledek z finally
        console.log(2); // nedostupné
    } finally {
        console.log(3);
        return false;   // overwrites the previous "return"
        console.log(4); // nedostupné
    }  
    console.log(5);     // nedostupné
}
f(); // console 0, 1, 3; returns false
```

## try ... catch ... finally

- `finally` může potlačit `throw` (opakované vyhození výjimky) v `catch`

```javascript
function f() {
    try {
        throw 'Ex 1';
    } catch(e) {
        console.log('catch 1 = ' + e);
        throw 'Ex 2';                // k tomuto nedojde
    } finally {
        console.log('finally');
        return 0;
    }
}

try {
    console.log('f() = ' + f());
}  catch(e) {
    console.log('catch 2 = ' + e);    // k tomuto nedojde
}
```

## Vnořování try ... catch

```javascript
try {
    console.log('try 1');
    try { 
        console.log('try 2');
        throw 'Ex';
    } catch(e) {
        console.log('catch 1 = ' + e);
    } finally {
        console.log('fin 1');
    }
} catch(e) {
    console.log('catch 2 = ' + e);
} finally {
    console.log('fin 2');
}

// try 1
// try 2
// catch 1 = Ex
// fin 1
// fin 2
```

## Vnořování try ... catch

- pokud vnitřní `try` nemá `catch`, musí mít `finally`

```javascript
try {
    console.log('try 1');
    try { console.log('try 2');
        throw 'Ex';
    } finally {
        console.log('fin 1');
    }
} catch(e) {
    console.log('catch 2 = ' + e);
} finally {
    console.log('fin 2');
}

// try 1
// try 2
// fin 1
// catch 2 = Ex
// fin 2
```

## `Error`

- předdefinované typy chyb

- `new Error([message[, fileName[, lineNumber]]])`
- typy chyb
  - `RangeError` - číslo je mimo platné meze
  - `ReferenceError` - např. při odkazu na neexistující proměnnou
  - `SyntaxError` - syntaktická chyba
  - `TypeError` - hodnota je neočekávaného typu
  - ...
- Pozn. `fileName` a `lineNumber` není standardizováno

## Použití `Error`

```javascript
try {
    throw new TypeError('Došlo k chybě')
} catch(e) {
    console.log(e instanceof TypeError);
    // true
    console.log(e.name + ': ' + e.message);
    // 'TypeError: Došlo k chybě'
}
```

## Vlastní chyba

```javascript
function MyError(message) {
    this.name = 'MyError';
    this.message = message || 'Chyba';
}
MyError.prototype = new Error;

try {
    throw new MyError;
} catch(e) {
    console.log(e instanceof Error);          // true
    console.log(e instanceof MyError);        // true
    console.log(e.name + ': ' + e.message);   // MyError: Chyba
}
```

## Zdroje

- [MDN Exception handling statements](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling#Exception_handling_statements)
- [MDN Error](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error)
- [MDN Error types](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error#Error_types)
