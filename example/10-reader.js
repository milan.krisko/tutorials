/**
 * Created by Pavel Švagr on 13. 4. 2017.
 */

//Přidá listener až po načtení stránky, předtím ještě není element FileReader známý
window.onload=function() {
    document.getElementById('fileReader').addEventListener('change', readFile);
}

function readFile (evt) {
    var files = evt.target.files;
    var file = files[0];
    var reader = new FileReader();

    //Mimo tuto funkci není možné soubory vypsat
    reader.onload = function() {

        //Změní obsah elementu s id "output" na přečtený text ze souboru.
        document.getElementById("output").innerHTML = this.result;
    }

    //Zavolá i předešlou funkci
    reader.readAsText(file);
}