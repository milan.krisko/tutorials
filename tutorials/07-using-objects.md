# Použití OOP - cvičení

> [Teoretická část](../08-using-objects.md)

## Serializace

(0,5 b)

- Implementujte metodu pro *serializaci* dat (na úrovni jednoho uživatele)
  - kvůli vzájemným vazbám nelze rovnou využít `JSON.stringify`
  - nahraďte vnořené objekty jejich ID
- Implementuje metodu pro *deserializaci* dat
  - tato metoda rekonstruuje ID na odkazy na objekty
- Ověřte, že graf objektů vzniklý po deserializaci je konzistentní (viz kontrola konzistence v minulém cvičení)

## Vyhledávání úkolů

(0,5 b)

Metody vytvořte do samostatného souboru (stejně jako modely), který budete načítat do hlavního programu

- `findTask(id)` - vrátí úkol podle zadaného ID.
- `findAllTasks(user [, done])` - vrátí pole všech úkolů zadaného uživatele, pokud je zadán argument `done` filtrujte podle jeho hodnoty (ne/splněné úkoly).
- `findMatchingTasks(substring[, user])` - vrátí všechny úkoly, jejichž název obsahuje podřetězec, pokud není zadán argument `user`, hledá u všech uživatelů.

## Přidání úkolu

(0,5 b)

V případě, že existuje nesplněný úkol se stejným popisem (`title`) nový úkol nepřidávejte.

- `addTask(group, title, due_date)` - přidá úkol do zadané skupiny.

## Odstranění úkolu

(0,5 b)

V případě, že úkol není splněný, neodstraní se.

- `deleteTask(id)` - odstraní úkol se zadaným ID
